package com.maybank.smartweb2.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

import com.maybank.smartweb2.service.CustomerUserDetailsService;

@Configuration
@EnableWebSecurity
public class MayAuth {
	
	@Autowired
	private CustomerUserDetailsService customerUserDetailsService;
	
	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
		authProvider.setUserDetailsService(customerUserDetailsService);
		authProvider.setPasswordEncoder(bCryptPasswordEncoder());
		return authProvider;
	}
	@Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity)throws Exception {
        httpSecurity.authorizeRequests().antMatchers("/employee")
        .hasAnyAuthority("regular");
        httpSecurity.authorizeRequests().antMatchers("/car")
        .hasAnyAuthority("admin");
        httpSecurity.authorizeRequests().antMatchers("/employee2")
        .hasAnyAuthority("admin");
//        httpSecurity.authorizeRequests().antMatchers("/api/user")
//        .permitAll();
//        httpSecurity.authorizeRequests().antMatchers("/api/user/save")
//        .permitAll();
        httpSecurity.csrf().disable();
        httpSecurity.authorizeRequests().antMatchers("/api/**")
        .authenticated().and().httpBasic();
//        httpSecurity.authorizeRequests().anyRequest().authenticated();
        httpSecurity.authorizeRequests().and().formLogin();

        return httpSecurity.build();
    }
//	@Bean
//	public UserDetailsService userDetailsService(BCryptPasswordEncoder bCryptPasswordEncoder) {
//		
//		InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
//		
//		manager.createUser(User.withUsername("abc")
//				.password(bCryptPasswordEncoder.encode("1234"))
//				.roles("regular").build());
//		
//		manager.createUser(User.withUsername("admin")
//				.password(bCryptPasswordEncoder.encode("1234"))
//				.roles("admin").build());
//		
//		return manager;
//	}
}
