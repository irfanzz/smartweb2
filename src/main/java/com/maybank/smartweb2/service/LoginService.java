package com.maybank.smartweb2.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maybank.smartweb2.entity.Role;
import com.maybank.smartweb2.entity.User;
import com.maybank.smartweb2.repository.RoleRepo;
import com.maybank.smartweb2.repository.UserRepo;

@Service
public class LoginService {
	
	@Autowired
	private UserRepo userRepo;
	@Autowired
	private RoleRepo roleRepo;
	
	public User findUserByUsername(String username) {
		return this.userRepo.findByUsername(username);
	}
	
	public void addUser(User user) {
		this.userRepo.save(user);
	}
	
	public List<User> findAll(){
		return this.userRepo.findAll();
	}
	
//	public List<Role> findRoleByUser(List<User> user){
//		return this.roleRepo.findRoleByUser(user);
//	}

}
