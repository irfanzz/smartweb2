package com.maybank.smartweb2.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.maybank.smartweb2.entity.Role;
import com.maybank.smartweb2.entity.User;
import com.maybank.smartweb2.repository.RoleRepo;
import com.maybank.smartweb2.repository.UserRepo;

@Service
@Transactional
public class InitDefaultUserAuth {
	
	@Autowired
	private RoleRepo roleRepo;
	@Autowired
	private UserRepo userRepo;
	
	@PostConstruct
	public void index() {
//		Create Role
		Role roleRegular = new Role();
		Role roleAdmin = new Role();
		
		roleRegular.setRole("regular");
		roleAdmin.setRole("admin");
		this.roleRepo.save(roleRegular);
		this.roleRepo.save(roleAdmin);
		
//		create user
		List<Role> listRole = new ArrayList<>();
		List<Role> userListRole = new ArrayList<>();
		listRole.add(roleAdmin);
		listRole.add(roleRegular);
		userListRole.add(roleRegular);
		
		User userAdmin = new User();
		userAdmin.setUsername("maybank");
		userAdmin.setEmail("maybank@maybank.com");
		userAdmin.setPassword(new BCryptPasswordEncoder().encode("123456"));
		userAdmin.setRoles(listRole);
		
		User userRegular = new User();
		userRegular.setUsername("irfan");
		userRegular.setEmail("irfan@dummy.dummy");
		userRegular.setPassword(new BCryptPasswordEncoder().encode("123456"));
		userRegular.setRoles(userListRole);
		
		this.userRepo.save(userAdmin);
		this.userRepo.save(userRegular);
	}

}
