package com.maybank.smartweb2.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.maybank.smartweb2.entity.Car;
import com.maybank.smartweb2.repository.CarRepo;

@Service
@Transactional
public class CarServiceImpl implements CarService{
	
	@Autowired
	private CarRepo carRepo;

	@Override
	public List<Car> getAll() {
		// TODO Auto-generated method stub
		return this.carRepo.findAll();
	}

	@Override
	public void save(Car car) {
		// TODO Auto-generated method stub
		this.carRepo.save(car);
	}

	@Override
	public void delete(Car car) {
		// TODO Auto-generated method stub
		this.carRepo.delete(car);
	}

	@Override
	public Optional<Car> getCarById(Long Id) {
		// TODO Auto-generated method stub
		return this.carRepo.findById(Id);
	}

}
