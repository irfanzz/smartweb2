package com.maybank.smartweb2.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import com.maybank.smartweb2.entity.Employee;

public interface EmployeeService {
	
	public List<Employee> getAll();
	public void save(Employee employee);
	public void delete(Employee employee);
	public Optional<Employee> getEmployeeById(Long Id);
	public Page<Employee> getAllPaginate(int pageNo, int pageSize, String field);
	public Page<Employee> getPaginateSearch(int pageNo, int pageSize, String field, String keyword);
}
