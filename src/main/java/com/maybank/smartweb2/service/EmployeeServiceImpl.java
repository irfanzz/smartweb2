package com.maybank.smartweb2.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.maybank.smartweb2.entity.Employee;
import com.maybank.smartweb2.repository.EmployeeRepo;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService{
	
	@Autowired
	private EmployeeRepo employeeRepo;

	@Override
	public List<Employee> getAll() {
		// TODO Auto-generated method stub
		return this.employeeRepo.findAll();
	}

	@Override
	public void save(Employee employee) {
		// TODO Auto-generated method stub
		this.employeeRepo.save(employee);
	}

	@Override
	public void delete(Employee employee) {
		// TODO Auto-generated method stub
		this.employeeRepo.delete(employee);
	}

	@Override
	public Optional<Employee> getEmployeeById(Long Id) {
		// TODO Auto-generated method stub
//		this.employeeRepo.getReferenceById(Id);
		return this.employeeRepo.findById(Id);
	}

	@Override
	public Page<Employee> getAllPaginate(int pageNo, int pageSize, String field) {
		// TODO Auto-generated method stub
		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(field).ascending());
		
		return this.employeeRepo.findAll(paging);
	}

	@Override
	public Page<Employee> getPaginateSearch(int pageNo, int pageSize, String field, String keyword) {
		// TODO Auto-generated method stub
		PageRequest paging = PageRequest.of(pageNo, pageSize, Sort.by(field).ascending());
		return this.employeeRepo.search(keyword, paging);
	}

}
