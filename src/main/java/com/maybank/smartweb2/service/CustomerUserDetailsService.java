package com.maybank.smartweb2.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.maybank.smartweb2.entity.Role;

@Service
public class CustomerUserDetailsService implements UserDetailsService{

	@Autowired
	private LoginService loginService;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		com.maybank.smartweb2.entity.User userLogin = loginService.findUserByUsername(username);
		List<GrantedAuthority> auths = new ArrayList<>();
//		auths.add(new SimpleGrantedAuthority("regular"));
//		auths.add(new SimpleGrantedAuthority("admin"));
		
		if(userLogin != null) {
			List<Role> roles = userLogin.getRoles();
			if(roles.size() > 0) {
				for(Role role: roles) {
					auths.add(new SimpleGrantedAuthority(role.getRole()));
				}
			}
			UserDetails user = User.withUsername(userLogin.getUsername())
					.password(userLogin.getPassword())
					.authorities(auths)
					.build();
			return user;
		}else {
			throw new UsernameNotFoundException("ERROR USER GA ADA");
		}
	}

}
