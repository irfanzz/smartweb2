package com.maybank.smartweb2.service;

import java.util.List;
import java.util.Optional;

import com.maybank.smartweb2.entity.Car;

public interface CarService {
	
	public List<Car> getAll();
	public void save(Car car);
	public void delete(Car car);
	public Optional<Car> getCarById(Long Id);

}
