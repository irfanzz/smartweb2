package com.maybank.smartweb2.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.smartweb2.entity.Car;

public interface CarRepo extends JpaRepository<Car, Long>{

}
