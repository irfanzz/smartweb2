package com.maybank.smartweb2.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.smartweb2.entity.User;

public interface UserRepo extends JpaRepository<User, Long>{

	User findByUsername(String username);

}
