package com.maybank.smartweb2.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.maybank.smartweb2.entity.Role;
import com.maybank.smartweb2.entity.User;

public interface RoleRepo extends JpaRepository<Role, Long>{

//	List<Role> findRoleByUser(List<User> user);

}
