package com.maybank.smartweb2.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.maybank.smartweb2.entity.Employee;

public interface EmployeeRepo extends JpaRepository<Employee, Long>{
//	@Query(value="SELECT * FROM employee p WHERE CONCAT(p.first_name, p.last_name, "
//			+ " p.address, p.contact) LIKE %:keyword", nativeQuery = true)
	@Query(value = "SELECT * FROM employee s where "
			+ "s.first_name iLIKE %:keyword% OR "
			+ "s.last_name iLIKE %:keyword% OR "
			+ "s.address iLIKE %:keyword% OR "
			+ "s.contact iLIKE %:keyword%", nativeQuery = true)
	public Page<Employee> search(@Param("keyword")String keyword, Pageable pageable);

}
