package com.maybank.smartweb2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.maybank.smartweb2.entity.Car;
import com.maybank.smartweb2.entity.Employee;
import com.maybank.smartweb2.repository.CarRepo;
import com.maybank.smartweb2.repository.EmployeeRepo;

@SpringBootApplication
//@Controller
public class Smartweb2Application implements ApplicationRunner{
	
	@Autowired
	private EmployeeRepo employeeRepo;
	@Autowired
	private CarRepo carRepo;
	
	public static void main(String[] args) {
		SpringApplication.run(Smartweb2Application.class, args);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("init app");
		// inisialisasi data ke model/entity employee
//		Employee employee = new Employee();
//		employee.setFirstName("Kylian");
//		employee.setLastName("Mbappe");
//		employee.setContact("087281719279");
//		employee.setAddress("Paris");
//		this.employeeRepo.save(employee);
		// inisialisasi data ke model/entity car
//		Car car = new Car();
//    	car.setBrand("Ferrari");
//    	car.setType("Sports");
//    	car.setPrice((double) 1500000000);
//    	car.setColor("Red");
//    	this.carRepo.save(car);
	}

}
