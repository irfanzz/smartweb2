package com.maybank.smartweb2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.maybank.smartweb2.entity.Car;
import com.maybank.smartweb2.entity.Employee;
import com.maybank.smartweb2.repository.EmployeeRepo;
import com.maybank.smartweb2.service.EmployeeService;

@Controller
@RequestMapping("/")
public class HomeController {
	
	@Autowired
	private EmployeeRepo employeeRepo;
	@Autowired
	private EmployeeService employeeService;
	
    @GetMapping
    public String index (Model model) {
    	
    	List<Employee> employees = this.employeeRepo.findAll();
//		Employee employee = new Employee();
//		employee.setFirstName("Kylian");
//		employee.setLastName("Mbappe");
//		employee.setContact("087281719279");
//		employee.setAddress("Paris");
        String springMessage = "hello view";
        model.addAttribute("employee",employees);
        model.addAttribute("SpringMessage",springMessage);
        return "home";
    }
//    @GetMapping("/car")
//    public String carPage(Model model) {
//    	Car car = new Car();
//    	car.setBrand("Ferrari");
//    	car.setType("Sports");
//    	car.setPrice((double) 1500000000);
//    	car.setColor("Red");
//    	String springmsg = "Ini Ferrari";
//    	model.addAttribute("car",car);
//    	model.addAttribute("springmsg", springmsg);
//    	return "car";
//    }
    @GetMapping("/employee2")
    public String employee2(Model model) {
    	
    	List<Employee> employees = this.employeeService.getAll();
        model.addAttribute("employees", employees);
        model.addAttribute("employeeForm", new Employee());
		model.addAttribute("page", employees);
    	return "employee2";
    }

}