package com.maybank.smartweb2.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.maybank.smartweb2.entity.Car;
import com.maybank.smartweb2.service.CarService;

@Controller
@RequestMapping("/car")
public class CarController {
	
	@Autowired
	private CarService carService;
	
	@GetMapping
	public String index(Model model) {
		
		List<Car> cars = carService.getAll();
		String springmsg = "Ini Mobil";
		model.addAttribute("carForm", new Car());
		model.addAttribute("car", cars);
		return "car";
	}
	
	@PostMapping("/save")
	public String save(Car car, RedirectAttributes redirectAttributes) {
		System.out.println("Car Brand: "+car.getBrand());
		this.carService.save(car);
		redirectAttributes.addFlashAttribute("success", "data inserted");
		return "redirect:/car";
	}
	
	@GetMapping("/delete")
	public String delete(@RequestParam("id") Long id) {
		Optional<Car> optional = this.carService.getCarById(id);
		if(optional.isPresent()) {
			this.carService.delete(optional.get());
		}
		return "redirect:/car";
	}

}
