package com.maybank.smartweb2.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.maybank.smartweb2.entity.Employee;
import com.maybank.smartweb2.repository.EmployeeRepo;
import com.maybank.smartweb2.service.EmployeeService;

@Controller
@RequestMapping("/employee")
public class EmployeeController {
	
	@Autowired
	private EmployeeService employeeService;
	
	@GetMapping
	public String index(
			@RequestParam(value= "pageNo", defaultValue = "0") int pageNo,
			@RequestParam(value = "pageSize", defaultValue = "1") int pageSize,
			@RequestParam(value = "sortField", defaultValue = "id") String sortField,
			@RequestParam(value = "keyword", defaultValue = "") String keyword,
			Model model) {
		
		Page<Employee> employees; 
		
		if(keyword == "") {
			employees = this.employeeService.getAllPaginate(pageNo, pageSize, sortField);
		}else {
			employees = this.employeeService.getPaginateSearch(pageNo, pageSize, sortField, keyword);
		}
		model.addAttribute("keyword", keyword);
		model.addAttribute("employeeForm", new Employee());
		model.addAttribute("page", employees);
		return "employee";
	}
	
	@PostMapping("/save")
	public String save(@Valid @ModelAttribute("employeeForm") Employee employee,
			BindingResult result,
			RedirectAttributes redirectAttributes,
			@RequestParam(value= "pageNo", defaultValue = "0") int pageNo,
			@RequestParam(value = "pageSize", defaultValue = "1") int pageSize,
			@RequestParam(value = "sortField", defaultValue = "id") String sortField,
			@RequestParam(value = "keyword", defaultValue = "") String keyword,
			Model model) {
		System.out.println("Employee Name: "+employee.getFirstName());
		if(result.hasErrors()) {
			Page<Employee> employees = this.employeeService.getAllPaginate(pageNo, pageSize, sortField);
//			List<Employee> employees = employeeService.getAll();
			model.addAttribute("page", employees);
			return "employee";
		}
//		employee.setFirstName(employee.getFirstName());
//		employee.setLastName(employee.getLastName());
//		employee.setContact(employee.getContact());
//		employee.setAddress(employee.getAddress());
		this.employeeService.save(employee);
		redirectAttributes.addFlashAttribute("success", "data inserted");
		return "redirect:/employee";
	}
	
	@GetMapping("/delete")
	public String delete(@RequestParam("id") Long id) {
		Optional<Employee> optional = this.employeeService.getEmployeeById(id);
		if(optional.isPresent()) {
			this.employeeService.delete(optional.get());
		}
		return "redirect:/employee";
	}
	
	@GetMapping("/edit-employee")
	public String edit(@RequestParam("id") Long id, Model model) {
		Optional<Employee> optional = this.employeeService.getEmployeeById(id);
		model.addAttribute("employeeForm", optional);
		return "edit-employee";
	}

}
