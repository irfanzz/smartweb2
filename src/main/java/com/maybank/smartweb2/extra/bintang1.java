package com.maybank.smartweb2.extra;

import org.springframework.boot.SpringApplication;

import com.maybank.smartweb2.Smartweb2Application;

public class bintang1 {
	
	public static void main(String[] args) {
		int i, j, n=9;
		System.out.println("\nSOAL 1, n = 9\n");
		for(i=1;i<=n;i++) {
			for(j=1;j<=n;j++) {
				if(j==i) {
					System.out.print("*");
				}else {
					System.out.print(" ");
				}
			}
			System.out.println("");
		}
//		SpringApplication.run(Smartweb2Application.class, args);\
		System.out.println("\nSOAL 2, n = 9\n");
		for(i=1;i<=n;i++) {
			for(j=n;j>=1;j--) {
				if(j==i) {
					System.out.print("*");
				}else {
					System.out.print(" ");
				}
			}
			System.out.println("");
		}
		System.out.println("\nSOAL 3, n = 9\n");
		for(i=1;i<=n;i++) {
			for(j=1;j<=n;j++) {
//				if((i==0&&(j==0||j==8))||(i==1&&(j==1||j==7))||(i==2&&(j==2||j==6))||(i==3&&(j==3||j==5))||(i==4&&j==4)
//						||(i==5&&(j==3||j==5))||(i==6&&(j==2||j==6))||(i==7&&(j==1||j==7))||(i==8&&(j==0||j==8))) {
//					System.out.print("*");
//				}else {
//					System.out.print("  ");
//				}
				if(i==j) {
					System.out.print("*");
				}else if(i+j==n+1) {
					System.out.print("*");
				}
				else {
					System.out.print(" ");
				}
			}
			System.out.println("");
		}
		System.out.println("\nSOAL 4, n = 9\n");
		for(i=1;i<=n;i++) {
			for(j=1;j<=n;j++) {
				if(j==5) {
					System.out.print("*");
				}else {
					if(i==5) {
						System.out.print("*");
					}else {
						if(i==j) {
							System.out.print("*");
						}else if(i+j==n+1) {
							System.out.print("*");
						}
						else {
							System.out.print(" ");
						}
					}
				}
			}
			System.out.println("");
		}
		System.out.println("\nSOAL 5, n = 9\n");
		for(i=1;i<=n;i++) {
			for(j=1;j<=n;j++) {
				if(j==1||j==9) {
					System.out.print("*");
				}else {
					if(i==1||i==9) {
						System.out.print("*");
					}else {
						if(i==j) {
							System.out.print("*");
						}else if(i+j==n+1) {
							System.out.print("*");
						}
						else {
							System.out.print(" ");
						}
					}
				}
			}
			System.out.println("");
		}
		System.out.println("\nSOAL 6, n = 9\n");
		for(i=1;i<=n;i++) {
			for(j=1;j<=i;j++) {
				System.out.print("*");
			}
			System.out.println("");
		}
		System.out.println("\nSOAL 7, n = 9\n");
		for(i=1;i<=n;i++) {
			for(j = i; j < n; j++) {
				System.out.print(" ");
			}
			for(j = 1; j <= i; j++) {
				System.out.print("*");
			}
			System.out.println("");
		}
		System.out.println("\nSOAL 8, n = 9\n");
		int s;
		for (i = n; i >= 1; i-=2) {
			for (s = i; s < n; s+=2) {
				System.out.print(" ");
			}
			for (j = 1; j <= i; j+=2) {
				System.out.print("*");
			}
			for (j = 1; j < i; j+=2) {
				System.out.print("*");
			}
			for (s = i; s < n; s+=2) {
				System.out.print(" ");
			}
			System.out.println("");
		}
		for (i = 1; i < n; i+=2) {
			for (s = i+2; s < n; s+=2) {
				System.out.print(" ");
			}
			for (j = 1; j <= i+2; j+=2) {
				System.out.print("*");
			}
			for (j = 1; j < i+2; j+=2) {
				System.out.print("*");
			}
			for (s = i+2; s < n; s+=2) {
				System.out.print(" ");
			}
			System.out.println("");
		}
		System.out.println("\nSOAL 9, n = 9\n");
		for (i = 1; i <= n; i+=2) {
			for(j = 1; j <= i; j+=2) {
				System.out.print("*");
			}
			for (s = i; s < n; s+=2) {
				System.out.print(" ");
			}
			for(s = i+1; s < n; s+=2) {
				System.out.print(" ");
			}
			for(j = 1; j <= i; j+=2) {
				System.out.print("*");
			}
			System.out.println("");
		}
		for (i = n; i >= 1; i-=2) {
			for(j = 1; j <= i; j+=2) {
				System.out.print("*");
			}
			for (s = i; s < n; s+=2) {
				System.out.print(" ");
			}
			for(s = i+1; s < n; s+=2) {
				System.out.print(" ");
			}
			for(j = 1; j <= i; j+=2) {
				System.out.print("*");
			}
			System.out.println("");
		}
		System.out.println("\nSOAL 9 Yang Benar\n");
		for (i = 0; i < n; i++) {
			for (j = 0; j < n; j++) {
				if (i <= j && j + i >= n - 1 || (i >= j && i + j <= n - 1)) {
					System.out.print("* ");
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}
		System.out.println("\nSOAL 10, n = 9\n");
		for (i = 0; i < n; i++) {
			for (j = 0; j < n; j++) {
				if ((i>=j && i<(n+1)/2) || (i<=j && i>=(n+1)/2-1)) {
					System.out.print("* ");
				}else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}
	}
}
